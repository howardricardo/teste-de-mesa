export const fases = [
  {
    1: {
      inicio: "let i = 0",
      condition: "i < 10",
      incremento: "i++",
      action: "console.log(i)",
      res_i: { condition: "i", res: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10] },
      res_console: {
        condition: "console.log",
        res: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "nda"],
      },
      res_condition: {
        condition: "condition",
        res: [
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          true,
          false,
        ],
      },
    },
  },
  {
    2: {
      inicio: "let i = 5",
      condition: "i > 0",
      incremento: "i--",
      action: "console.log(i)",
    },
  },
  {
    3: {
      inicio: "let i = 0",
      condition: "i < 10",
      incremento: "i++",
      action: "console.log(i * i)",
    },
  },
  {
    4: {
      inicio: "let i = 0",
      condition: "i < 5",
      incremento: "i++",
      action: "console.log(i ** i)",
    },
  },
  {
    5: {
      inicio: "let i = 10",
      condition: "i >= 0",
      incremento: "i--",
      action: "console.log(i - i * 7)",
    },
  },
];
