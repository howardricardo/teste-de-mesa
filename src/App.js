import "./App.css";
import Loop from "./components/loop1/loop1";
import LoopTwo from "./components/loop1/loop2";
import LoopThree from "./components/loop1/loop3";
import LoopFour from "./components/loop1/loop4";
import LoopFive from "./components/loop1/loop5";
import { fases } from "./data/fases";

import { Route, Switch, Link } from "react-router-dom";

const App = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Lista de exercícios: Teste de mesa</h1>
        <div>
          <Link to="/loop1">
            <button>1</button>
          </Link>
          <Link to="/loop2">
            <button>2</button>
          </Link>
          <Link to="/loop3">
            <button>3</button>
          </Link>
          <Link to="/loop4">
            <button>4</button>
          </Link>
          <Link to="/loop5">
            <button>5</button>
          </Link>
        </div>
        <Switch>
          <Route path="/loop1">
            <Loop
              inicio="let i = 0"
              condition="i < 10"
              incremento="i++"
              action="console.log(i)"
            />
          </Route>
          <Route path="/loop2">
            <LoopTwo
              inicio="let i = 5"
              condition="i > 0"
              incremento="i--"
              action="console.log(i)"
            />
          </Route>
          <Route path="/loop3">
            <LoopThree
              inicio="let i = 0"
              condition="i < 10"
              incremento="i++"
              action="console.log(i * i)"
            />
          </Route>
          <Route path="/loop4">
            <LoopFour
              inicio="let i = 0"
              condition="i < 5"
              incremento="i++"
              action="console.log(i ** i)"
            />
          </Route>
          <Route path="/loop5">
            <LoopFive
              inicio="let i = 10"
              condition="i >= 0"
              incremento="i--"
              action="console.log(i - i * 7)"
            />
          </Route>
        </Switch>
      </header>
    </div>
  );
};

export default App;
