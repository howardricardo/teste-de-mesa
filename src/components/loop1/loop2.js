import React from 'react'
import './style.css'
import Table from './table/tableTwo.js'
import { Link } from "react-router-dom";

const LoopTwo = (props) => {
    return (
        <div>
            <div className='loop'>
            <h3>Exercício 2: </h3>
                <span><span style={{color: 'purple'}}>for</span> ({props.inicio}; {props.condition} ; {props.incremento}) &#123;</span>
                <span className='conteudo'>{props.action}</span>
                <span>&#125;</span>
            </div>
            <div className='table'>
            <p className='description'>~~~Se não houver resultado, responda "nda"~~~</p>
                <div className='testeDeMesa'>
                    <Table variaveis={['i']} vezes={[1, 2, 3, 4, 5, 6]} aparece={true}/>
                    <Table variaveis={['console']} vezes={[1, 2, 3, 4, 5, 6]}/>
                    <Table variaveis={['condição']} vezes={[1, 2, 3, 4, 5, 6]}/>
                </div>
                <Link to='./loop3'><button className='submit'>Próximo</button></Link>
            </div>
        </div>
    )
}

export default LoopTwo
