import "../style.css";

const Inputs = (props) => {
  return (
    <div>
      <input
        name={props.Key}
        placeholder={props.inputs}
        className={props.className}
        onChange={props.onChange}
        value={props.value}
      ></input>
    </div>
  );
};

export default Inputs;
