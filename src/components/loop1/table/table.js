import { useState } from "react";
import Inputs from "./inputs";
import "../style.css";
import { fases } from "../../../data/fases";

const Table = (props) => {
  const [variaveis] = useState(props.variaveis);
  const [vezes] = useState(props.vezes);
  const [aparece] = useState(props.aparece);

  const campo = (input, condition, answer) => {
    const res = input.target.value;

    if (input.target.placeholder === condition) {
      if (res === answer.toString()) {
        input.target.className = "inputsCerto";
      } else {
        input.target.className = "inputs";
      }
    }
  };

  const verifica = (input) => {
    const fase = fases[0][1];
    const key = input.target.name;
    const res_i = fase.res_i;
    const res_console = fase.res_console;
    const res_condition = fase.res_condition;
    campo(input, res_i.condition, res_i.res[key]);
    campo(input, res_console.condition, res_console.res[key]);
    campo(input, res_condition.condition, res_condition.res[key]);
  };
  return (
    <div>
      {variaveis.map((elt, index) => (
        <span key={index} className="topo">
          {elt}{" "}
        </span>
      ))}
      <br />
      {vezes.map((_elt, index) => (
        <span key={index} className="mesa">
          {aparece && index + 1 + "° volta: "}
          <Inputs
            Key={index}
            inputs={variaveis}
            className={`inputs`}
            onChange={(e) => verifica(e)}
          />
        </span>
      ))}
    </div>
  );
};

export default Table;
