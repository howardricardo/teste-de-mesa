import { useState } from 'react'
import Inputs from './inputs'
import '../style.css'


const Table = (props) => {
    const [variaveis] = useState(props.variaveis)
    const [vezes] = useState(props.vezes)
    const [aparece] = useState(props.aparece)
    

    const campo = (input, condition, answer) => {
        if (input.target.placeholder === condition) {
            if (input.target.value === answer) {
                input.target.className = 'inputsCerto'
            } else {
                input.target.className = 'inputs'
            }
        }
    }

    const verifica = (input) => {
        campo(input, 'i 1', '0')
        campo(input, 'i 2', '1')
        campo(input, 'i 3', '2')
        campo(input, 'i 4', '3')
        campo(input, 'i 5', '4')
        campo(input, 'i 6', '5')
        campo(input, 'i 7', '6')
        campo(input, 'i 8', '7')
        campo(input, 'i 9', '8')
        campo(input, 'i 10', '9')
        campo(input, 'i 11', '10')
        campo(input, 'console 1', '0')
        campo(input, 'console 2', '1')
        campo(input, 'console 3', '4')
        campo(input, 'console 4', '9')
        campo(input, 'console 5', '16')
        campo(input, 'console 6', '25')
        campo(input, 'console 7', '36')
        campo(input, 'console 8', '49')
        campo(input, 'console 9', '64')
        campo(input, 'console 10', '81')
        campo(input, 'console 11', 'nda')
        campo(input, 'condição 1', 'true')
        campo(input, 'condição 2', 'true')
        campo(input, 'condição 3', 'true')
        campo(input, 'condição 4', 'true')
        campo(input, 'condição 5', 'true')
        campo(input, 'condição 6', 'true')
        campo(input, 'condição 7', 'true')
        campo(input, 'condição 8', 'true')
        campo(input, 'condição 9', 'true')
        campo(input, 'condição 10', 'true')
        campo(input, 'condição 11', 'false')
    }
    return (
        <div>
            {variaveis.map((elt, index) => <span key={index} className='topo'>{elt} </span>)}<br/>
            {vezes.map((elt, index) => <span key={index} className='mesa'>{aparece && elt + '° volta: '}<Inputs inputs={variaveis + ' ' + elt} className={`inputs`} onChange={verifica}/></span>)}
        </div>
    )
}

export default Table 
