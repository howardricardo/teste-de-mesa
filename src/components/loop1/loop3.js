import React from 'react'
import './style.css'
import Table from './table/tableThree.js'
import { Link } from "react-router-dom";

const LoopThree = (props) => {
    return (
        <div>
            <div className='loop'>
            <h3>Exercício 3: </h3>
                <span><span style={{color: 'purple'}}>for</span> ({props.inicio}; {props.condition} ; {props.incremento}) &#123;</span>
                <span className='conteudo'>{props.action}</span>
                <span>&#125;</span>
            </div>
            <div className='table'>
            <p className='description'>~~~Se não houver resultado, responda "nda"~~~</p>
                <div className='testeDeMesa'>
                    <Table variaveis={['i']} vezes={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]} aparece={true}/>
                    <Table variaveis={['console']} vezes={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}/>
                    <Table variaveis={['condição']} vezes={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]}/>
                </div>
                <Link to='./loop4'><button className='submit'>Próximo</button></Link>
            </div>
        </div>
    )
}

export default LoopThree
